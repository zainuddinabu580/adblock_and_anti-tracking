/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package tunnel

import java.net.InetSocketAddress
import java.nio.ByteBuffer

internal class BlockaTunnelFiltering(
        private val dnsServers: List<InetSocketAddress>,
        private val blockade: Blockade,
        private val loopback: () -> Any,
        private val errorOccurred: (String) -> Any,
        private val buffer: ByteBuffer
) {
    fun handleFromDevice(fromDevice: ByteArray, length: Int) = false

    fun handleToDevice(destination: ByteBuffer, length: Int) = false

    fun restart() {}
}
