/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core

import tunnel.TunnelConfig

fun setTunnelPersistenceSource() {
    Register.sourceFor(TunnelConfig::class.java, default = TunnelConfig(adblocking = false),
            source = NoAdblockingSource())
}

private class NoAdblockingSource: PaperSource<TunnelConfig>("tunnel:config") {
    override fun <T> get(classOfT: Class<T>, id: String?): T? {
        val tun = super.get(classOfT, id) as TunnelConfig?
        return tun?.copy(adblocking = false) as T?
    }
}
