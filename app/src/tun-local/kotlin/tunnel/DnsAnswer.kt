/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package tunnel

import com.github.salomonbrys.kodein.instance
import core.*
import gs.property.I18n
import org.blokada.R
import org.xbill.DNS.*
import java.net.InetAddress

/*
persistence for the setting, which determines which type of answer Blokada will give if a domain is blocked.
If hostNotFoundAnswer is true it will send the "classic" deny response. If it's false Blokada will instead
send a answer resolving the blocked domain to 127.1.1.1 .
*/
data class DnsAnswerState(
        val hostNotFoundAnswer: Boolean
)

fun generateDnsAnswer(dnsMessage: Message, denyResponse: SOARecord){
    if(get(DnsAnswerState::class.java).hostNotFoundAnswer){
        dnsMessage.addRecord(denyResponse, Section.AUTHORITY)
    }else{
        dnsMessage.addRecord(
                ARecord(Name(dnsMessage.question.name.toString(false)),
                        DClass.IN,
                        5, //5 sec ttl
                        InetAddress.getByAddress(byteArrayOf(127, 1, 1, 1))),
                Section.ANSWER
        )
    }
}

class DnsAnswerTypeVB(
        private val ktx: AndroidKontext,
        private val i18n: I18n = ktx.di().instance(),
        onTap: (SlotView) -> Unit
) : SlotVB(onTap) {

    override fun attach(view: SlotView) {
        view.enableAlternativeBackground()
        view.type = Slot.Type.INFO
        view.content = Slot.Content(
                icon = ktx.ctx.getDrawable(R.drawable.ic_feedback),
                label = i18n.getString(R.string.slot_dns_answer_label),
                description = i18n.getString(R.string.slot_dns_answer_description),
                switched = !get(DnsAnswerState::class.java).hostNotFoundAnswer
        )
        view.onSwitch = { Register.set(DnsAnswerState::class.java, DnsAnswerState(!it)) }
    }

}

