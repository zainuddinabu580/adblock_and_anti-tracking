/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package tunnel

import com.google.android.material.snackbar.Snackbar
import core.Resource
import core.activityRegister
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import org.blokada.R

fun showSnack(resource: Resource) = GlobalScope.async {
    delay(1000)
    GlobalScope.async(Dispatchers.Main) {
        activityRegister.getParentView()?.run {
            if (resource.hasResId()) showSnack(resource.getResId())
            else {
                val snack = Snackbar.make(this, resource.getString(), 5000)
                snack.view.setBackgroundResource(R.drawable.snackbar)
                snack.view.setPadding(32, 32, 32, 32)
                snack.setAction(R.string.menu_ok, { snack.dismiss() })
                snack.show()
            }
        }
    }
}

fun showSnack(msgResId: Int) = GlobalScope.async {
    delay(1000)
    async(Dispatchers.Main) {
        activityRegister.getParentView()?.run {
            val snack = Snackbar.make(this, msgResId, 5000)
            snack.view.setBackgroundResource(R.drawable.snackbar)
            snack.view.setPadding(32, 32, 32, 32)
            snack.setAction(R.string.menu_ok, { snack.dismiss() })
            snack.show()
        }
    }
}
