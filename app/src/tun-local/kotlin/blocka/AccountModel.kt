/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package blocka

import android.os.Build
import java.util.*

data class CurrentAccount(
        val id: String = "",
        val activeUntil: Date = Date(0),
        val privateKey: String = "",
        val publicKey: String = "",
        val lastAccountCheck: Long = 0,
        val accountOk: Boolean = false,
        val migration: Int = 0,
        val unsupportedForVersionCode: Int = 0
) {
    override fun toString(): String {
        // No account ID and private key
        return "CurrentAccount(activeUntil=$activeUntil, publicKey='$publicKey', lastAccountCheck=$lastAccountCheck, accountOk=$accountOk, migration=$migration)"
    }
}

data class CurrentLease(
        val gatewayId: String = "",
        val gatewayIp: String = "",
        val gatewayPort: Int = 0,
        val gatewayNiceName: String = "",
        val vip4: String = "",
        val vip6: String = "",
        val leaseActiveUntil: Date = Date(0),
        val leaseOk: Boolean = false,
        val migration: Int = 0
)

data class BlockaVpnState(
        val enabled: Boolean
)


typealias AccountId = String
typealias ActiveUntil = Date

fun ActiveUntil.expired() = this.before(Date())

internal val defaultDeviceAlias = "%s-%s".format(Build.MANUFACTURER, Build.DEVICE)
