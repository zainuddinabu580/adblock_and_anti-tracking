/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package blocka

import core.v

internal class AccountManager(
        internal var state: CurrentAccount,
        val newAccountRequest: () -> AccountId? = { throw Exception("not implemented") },
        val getAccountRequest: (AccountId) -> ActiveUntil = { throw Exception("not implemented") },
        val generateKeypair: () -> Pair<String, String> = { throw Exception("not implemented") },
        val accountValid: () -> Unit = {}
) {

    private var lastAccountRequest = 0L

    fun sync(force: Boolean) {
        ensureAccount()
        ensureKeypair()
        when {
            !force && lastAccountRequest + 3600 * 1000 > System.currentTimeMillis() -> {
                v("skipping account request, done one recently")
            }
            else -> {
                try {
                    val activeUntil = getAccountRequest(state.id)
                    state = state.copy(
                            activeUntil = activeUntil,
                            accountOk = !activeUntil.expired(),
                            lastAccountCheck = System.currentTimeMillis()
                    )
                    lastAccountRequest = System.currentTimeMillis()
                    if (!activeUntil.expired()) accountValid()
                } catch (ex: Exception) {
                    state = state.copy(accountOk = state.accountOk)
                    throw Exception("failed to get account request", ex)
                }
            }
        }
    }

    fun restoreAccount(newId: AccountId) {
        val activeUntil = getAccountRequest(newId)
        state = state.copy(
                id = newId,
                activeUntil = activeUntil,
                accountOk = true
        )
        if (!activeUntil.expired()) accountValid()
    }

    private fun ensureAccount() {
        if (state.id.isBlank()) {
            // New account
            val id = newAccountRequest()
            if (id?.isBlank() != false) throw Exception("failed to request new account")
            state = state.copy(
                    id = id,
                    accountOk = false,
                    lastAccountCheck = System.currentTimeMillis()
            )
        }
    }

    private fun ensureKeypair() {
        if (state.publicKey.isNotBlank() and state.privateKey.isNotBlank()) return
        val (private, public) = generateKeypair()
        state = state.copy(
                privateKey = private,
                publicKey = public
        )
    }
}
