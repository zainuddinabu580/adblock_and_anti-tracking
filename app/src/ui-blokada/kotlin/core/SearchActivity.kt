/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package core

import android.app.Activity
import android.os.Bundle
import core.bits.EnterSearchVB
import org.blokada.R


class SearchActivity : Activity(){

    private val stepView by lazy { findViewById<VBStepView>(R.id.view) }
    private val ktx = ktx("SearchActivity")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vbstepview)

        val searchVB = EnterSearchVB(ktx) { s -> callback(s); finish() }

        stepView.pages = listOf(
                searchVB
        )
    }

    companion object{
        private var callback : (String) -> Unit = {}

        fun setCallback(c:(String) -> Unit){
            callback = c
        }
    }
}
