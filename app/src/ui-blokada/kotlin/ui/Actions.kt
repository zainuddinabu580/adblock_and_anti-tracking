/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package ui

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.widget.Toast
import blocka.BlockaVpnState
import com.github.salomonbrys.kodein.instance
import com.twofortyfouram.locale.sdk.client.receiver.AbstractPluginSettingReceiver
import core.*
import core.bits.menu.shareLog
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import org.blokada.R

class SendLogActivity : Activity() {

    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        super.onCreate(savedInstanceState)
        //initGlobal(this, failsafe = true)
        shareLog(this)
        GlobalScope.async {
            delay(2000)
            finish()
        }
        //System.exit(0)
    }
}

val EVENT_KEY_SWITCH = "blokada_switch"
val EVENT_KEY_SWITCH_DNS = "blokada_switch_dns"
val EVENT_KEY_SWITCH_BLOCKA_VPN = "blokada_switch_blocka_vpn"

class SwitchAppReceiver : AbstractPluginSettingReceiver() {

    override fun isAsync(): Boolean {
        return false
    }

    override fun firePluginSetting(ctx: Context, bundle: Bundle) {
        when {
            bundle.containsKey(EVENT_KEY_SWITCH) -> switchApp(ctx, bundle.getBoolean(EVENT_KEY_SWITCH))
            bundle.containsKey(EVENT_KEY_SWITCH_BLOCKA_VPN) -> switchBlockaVpn(ctx, bundle.getBoolean(EVENT_KEY_SWITCH_BLOCKA_VPN))
            bundle.containsKey(EVENT_KEY_SWITCH_DNS) -> switchDns(ctx, bundle.getBoolean(EVENT_KEY_SWITCH_DNS))
            else -> e("unknown app intent")
        }
    }

    private fun switchApp(ctx: Context, on: Boolean) {
        try {
            v("switching app from intent", on)

            val ktx = ctx.ktx("switch-intent")
            val tunnelEvents = ktx.di().instance<Tunnel>()
            val wasOn = tunnelEvents.enabled()

            if (wasOn != on) {
                tunnelEvents.enabled %= on
                val msg = if (on) R.string.notification_keepalive_activating
                else R.string.notification_keepalive_deactivating
                Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show()
            }
        } catch (ex: Exception) {
            e("invalid switch app intent", ex)
        }
    }

    private fun switchDns(ctx: Context, on: Boolean) {
        try {
            v("switching dns from intent", on)

            val ktx = ctx.ktx("switch-intent")
            val dns = ktx.di().instance<Dns>()
            val wasOn = dns.enabled()

            if (wasOn != on) {
                entrypoint.onSwitchDnsEnabled(on)
//                val msg = if (switch) R.string.notification_keepalive_activating
//                else R.string.notification_keepalive_deactivating
//                Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show()
            }
        } catch (ex: Exception) {
            e("invalid switch dns intent", ex)
        }
    }

    private fun switchBlockaVpn(ctx: Context, on: Boolean) {
        try {
            v("switching blocka vpn from intent", on)

            val wasOn = get(BlockaVpnState::class.java).enabled

            if (wasOn != on) {
                entrypoint.onVpnSwitched(on)
//                val msg = if (switch) R.string.notification_keepalive_activating
//                else R.string.notification_keepalive_deactivating
//                Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show()
            }
        } catch (ex: Exception) {
            e("invalid switch blocka vpn intent", ex)
        }
    }

    override fun isBundleValid(bundle: Bundle)
            = bundle.containsKey(EVENT_KEY_SWITCH)
            || bundle.containsKey(EVENT_KEY_SWITCH_DNS)
            || bundle.containsKey(EVENT_KEY_SWITCH_BLOCKA_VPN)

}
