/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package ui

import android.app.Activity
import android.content.ComponentName
import android.content.Intent
import android.net.Uri
import android.widget.FrameLayout
import android.widget.TextView
import androidx.browser.customtabs.CustomTabsClient
import androidx.browser.customtabs.CustomTabsIntent
import androidx.browser.customtabs.CustomTabsServiceConnection
import blocka.blokadaUserAgent
import com.github.salomonbrys.kodein.LazyKodein
import core.ktx
import core.modalManager
import gs.presentation.WebDash
import gs.property.IProperty
import gs.property.IWhen
import org.blokada.R
import java.net.URL
import java.net.URLEncoder

abstract class AbstractWebActivity : Activity() {

    private val container by lazy { findViewById<FrameLayout>(R.id.view) }
    private val close by lazy { findViewById<TextView>(R.id.close) }
    private val openBrowser by lazy { findViewById<android.view.View>(R.id.browser) }
    private val ktx = ktx("AbstractWebActivity")

    lateinit var targetUrl: IProperty<URL>

    private val dash by lazy {
        WebDash(LazyKodein(ktx.di), targetUrl, reloadOnError = true,
                javascript = true, forceEmbedded = true, big = true)
    }

    private var view: android.view.View? = null
    private var listener: IWhen? = null
    private var exitedToBrowser = false

    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.subscription_container)

        if (bound || bindChromeTabs()) {
            val target = targetUrl().toExternalForm()
            val url = if (target.contains("?")) {
                target + "&user-agent=" + URLEncoder.encode(blokadaUserAgent(this, true), "utf-8")
            } else {
                target + "?user-agent=" + URLEncoder.encode(blokadaUserAgent(this, true), "utf-8")
            }
            val builder = CustomTabsIntent.Builder()
            val customTabsIntent = builder.build()

            customTabsIntent.launchUrl(this, Uri.parse(url))
            unbindService(connection)
            finish()
        } else {
            view = dash.createView(this, container)
            listener = targetUrl.doOnUiWhenSet().then {
                view?.run { dash.attach(this) }
            }
            container.addView(view)
            close.setOnClickListener { finish() }
            openBrowser.setOnClickListener {
                try {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    intent.data = Uri.parse(targetUrl().toString())
                    startActivity(intent)
                    exitedToBrowser = true
                } catch (e: Exception) {}
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        view?.run { dash.detach(this) }
        container.removeAllViews()
        targetUrl.cancel(listener)
        modalManager.closeModal()
    }

    override fun onStart() {
        super.onStart()

        if (exitedToBrowser) {
            exitedToBrowser = false
            finish()
        }
    }

    private val CUSTOM_TAB_PACKAGE_NAME = "com.android.chrome"
    private var bound = false

    var connection: CustomTabsServiceConnection = object : CustomTabsServiceConnection() {
        override fun onCustomTabsServiceConnected(name: ComponentName, client: CustomTabsClient) {
            bound = true
        }

        override fun onServiceDisconnected(name: ComponentName) {
            bound = false
        }
    }

    fun bindChromeTabs() = CustomTabsClient.bindCustomTabsService(this, CUSTOM_TAB_PACKAGE_NAME, connection)
}
