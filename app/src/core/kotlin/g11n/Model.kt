/*
Copyright 2020 blokada of https://blokada.org represented by
KAROL GUSAK
JOHNNY BERGSTRÖM
PASCAL SINGLE
ANTONIO GALEA

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package g11n

import core.Time
import core.Url

typealias Prefix = String
typealias Key = String
typealias Translation = String
typealias Translations = List<Pair<Key, Translation>>

fun emptyTranslations() = emptyList<Pair<Key, Translation>>()

data class TranslationStore(
        val cache: Map<Url, Time> = emptyMap()
) {
    fun get(url: Url) = cache.getOrElse(url, { 0 })
    fun put(url: Url) = TranslationStore(cache.plus(url to System.currentTimeMillis()))
}
