package ads;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.mopub.mobileads.MoPubErrorCode;

/**
 * Created by Sönke Gissel on 26.06.2019.
 */
public class MoPubInterstitial extends Ad {

    private com.mopub.mobileads.MoPubInterstitial mInterstitial;
    private Context mContext;
    private MutableLiveData<Boolean> isInterstitialReady = new MutableLiveData<>();

    public MoPubInterstitial(Context context) {
        super(context);
        this.mContext = context;
        mInterstitial = new com.mopub.mobileads.MoPubInterstitial((Activity) mContext, getAdUnitId(this));

        InterstitialAdListener listener = new InterstitialAdListener();
        mInterstitial.setInterstitialAdListener(listener);
    }

    // GETTER
    public com.mopub.mobileads.MoPubInterstitial getInterstitialAd() {
        return mInterstitial;
    }

    @Override
    public void destroy() { mInterstitial.destroy(); }

    @Override
    public void load() {
        mInterstitial.load();
    }

    private class InterstitialAdListener implements com.mopub.mobileads.MoPubInterstitial.InterstitialAdListener {

        @Override
        public void onInterstitialLoaded(com.mopub.mobileads.MoPubInterstitial interstitial) {
            Log.d(this.getClass().getSimpleName(), "onInterstitialLoaded abc.");
            isInterstitialReady.setValue(true);
        }

        @Override
        public void onInterstitialFailed(com.mopub.mobileads.MoPubInterstitial interstitial, MoPubErrorCode errorCode) {
            Log.d(this.getClass().getSimpleName(), "onInterstitialFailed. With error "+errorCode.toString());
            isInterstitialReady.setValue(false);
        }

        @Override
        public void onInterstitialShown(com.mopub.mobileads.MoPubInterstitial interstitial) {
            Log.d(this.getClass().getSimpleName(), "onInterstitialShown.");
            isInterstitialReady.setValue(false);
        }

        @Override
        public void onInterstitialClicked(com.mopub.mobileads.MoPubInterstitial interstitial) {
            Log.d(this.getClass().getSimpleName(), "onInterstitialClicked.");
            isInterstitialReady.setValue(false);
        }

        @Override
        public void onInterstitialDismissed(com.mopub.mobileads.MoPubInterstitial interstitial) {
            Log.d(this.getClass().getSimpleName(), "onInterstitialDismissed.");
            isInterstitialReady.setValue(false);
        }
    }

    public MutableLiveData<Boolean> getIsInterstitialReady() {
        return isInterstitialReady;
    }
}
